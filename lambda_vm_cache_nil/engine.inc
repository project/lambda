<?php

class nil_lambda_cache_engine implements lambda_cache_engine {

  /**
   * Implementation of lambda_cache_engine::__construct().
   *
   * @param array $parameters
   */
  public function __construct(array $parameters) {
  }

  /**
   * Implementation of lambda_cache_engine::is_succeed().
   *
   * @return boolean
   */
  public function init_succeed() {
    return TRUE;
  }

  /**
   * Implementation of lambda_cache_engine::get().
   *
   * @param string $key
   * @return mixed
   */
  public function get($key) {
    return NULL;
  }

  /**
   * Implementation of lambda_cache_engine::set().
   *
   * @param string $key
   * @param mixed $value
   */
  public function set($key, $value) {
  }

  /**
   * Implementation of lambda_cache_engine::flush().
   */
  public function flush(){
  }

  /**
   * Implementation of lambda_cache_engine::__destruct().
   */
  public function __destruct() {
  }

  /**
   * Implementation of lambda_cache_engine::get_instance_parameters().
   *
   * @return array
   */
  public static function get_instance_parameters() {
    return array();
  }
}