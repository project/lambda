<?php

function _lambda_node_var_dump($data) {
  $out  = '<pre>';

  ob_start();
  var_dump($data);
  $out .= ob_get_contents();
  ob_end_clean();

  $out .= '</pre>';

  return $out;
}

function _lambda_node_parse_tree($node) {
  return _lambda_node_var_dump(LambdaVM::getInstance()->getParseTree($node->nid));
}

function _lambda_node_compile_debug($nid) {
  return '';
}

function _lambda_node_run_debug($nid) {
  return '';
}