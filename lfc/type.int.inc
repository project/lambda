<?php

class int_type extends _type {
	
	public static function get_name() {
		return 'int';
	}

	public static function is_me($var) {
		return is_int($var);
	}

	public static function is_convertable($var) {
		return is_numeric($var);
	}

	public static function convert($var) {
		return (int)$var;
	}
}
