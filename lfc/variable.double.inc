<?php

/**
 * Variable representation for the double type.
 */
class double_variable extends variable {

  /**
   * Constructor.
   *
   * Named arguments:
   *  - double $value: the value of this variable
   *
   * @param array $arguments
   */
  public function __construct(array $arguments) {
    $this->value = (double)$arguments['value'];
  }

  /**
   * Implementation of gui_info::get_constructor_parameters().
   *
   * @return array
   */
  public static function get_constructor_parameters() {
    $arr = array();

    $arr['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
    );

    return $arr;
  }

  /**
   * Implementation of gui_info::validate_constructor_parameters().
   *
   * @return array
   */
  public static function validate_constructor_parameters($values) {
    return ( double_type::is_me($values['value']) || double_type::is_convertable($values['value']) ) ?
      array() : array('value');
  }
}