<?php

/**
 * Variable representation for the bool type.
 */
class bool_variable extends variable {

  /**
   * Constructor.
   *
   * Named arguments:
   *  - bool $value: the value of this variable
   *
   * @param array $arguments
   */
  public function __construct(array $arguments) {
    $this->value = (bool)$arguments['value'];
  }

  /**
   * Implementation of gui_info::get_constructor_parameters().
   *
   * @return array
   */
  public static function get_constructor_parameters() {
    $arr = array();

    $arr['value'] = array(
      '#type' => 'radios',
      '#title' => t('Value'),
      '#options' => array('1' => t('True'), '0' => t('False')),
    );

    return $arr;
  }

  /**
   * Implementation of gui_info::validate_constructor_parameters().
   *
   * @return array
   */
  public static function validate_constructor_parameters($values) {
    return ( bool_type::is_me($values['value']) || bool_type::is_convertable($values['value']) ) ?
      array() : array('value');
  }
}