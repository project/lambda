<?php

class double_type extends _type {

	public static function get_name() {
		return 'double';
	}

	public static function is_me($var) {
		return is_double($var);
	}

	public static function is_convertable($var) {
		return is_numeric($var);
	}

	public static function convert($var) {
		return (double)$var;
	}
}
