<?php

class bool_type extends _type {
	
	public static function get_name() {
		return 'bool';
	}

	public static function is_me($var) {
		return is_bool($var);
	}

	public static function is_convertable($var) {
		return TRUE;
	}

	public static function convert($var) {
		return (bool)$var;
	}
}
