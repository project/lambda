<?php

/**
 * Variable representation for the string type.
 */
class string_variable extends variable {

  /**
   * Constructor.
   *
   * Named arguments:
   *  - string $value: the value of this variable
   *
   * @param array $arguments
   */
  public function __construct(array $arguments) {
    $this->value = (string)$arguments['value'];
  }

  /**
   * Implementation of gui_info::get_constructor_parameters().
   *
   * @return array
   */
  public static function get_constructor_parameters() {
    $arr = array();

    $arr['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
    );

    return $arr;
  }

  /**
   * Implementation of gui_info::validate_constructor_parameters().
   *
   * @return array
   */
  public static function validate_constructor_parameters($values) {
    return array();
  }
}