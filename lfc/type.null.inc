<?php

class null_type extends _type {

	public static function get_name() {
		return 'null';
	}

	public static function is_me($var) {
		return $var === NULL;
	}

	public static function is_convertable($var) {
		return TRUE;
	}

	public static function convert($var) {
		return NULL;
	}
}
