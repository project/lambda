<?php

class string_type extends _type {
	
	public static function get_name() {
		return 'string';
	}

	public static function is_me($var) {
		return is_string($var);
	}

	public static function is_convertable($var) {
		return TRUE;
	}

	public static function convert($var) {
		return (string)$var;
	}
}
