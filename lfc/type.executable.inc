<?php

abstract class executable_type extends _type {

	public static function get_name() {
		return 'executable';
	}

	public static function is_me($var) {
		return $var instanceof executable_type;
	}

	public static function is_convertable($var) {
		return FALSE;
	}

	public static function convert($var) {
		return NULL;
	}

	public abstract function execute();
}
