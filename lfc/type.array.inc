<?php

class array_type extends _type {
	
	public static function get_name() {
		return 'array';
	}

	public static function is_me($var) {
		return is_array($var);
	}

	public static function is_convertable($var) {
		return $this->is_me($var);
	}

	public static function convert($var) {
		return (array)$var;
	}
}
