<?php

class float_type extends _type {
	
	public static function get_name() {
		return 'float';
	}

	public static function is_me($var) {
		return is_float($var);
	}

	public static function is_convertable($var) {
		return is_numeric($var);
	}

	public static function convert($var) {
		return (float)$var;
	}
}
