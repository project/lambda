<?php

class memcache_lambda_cache_engine implements lambda_cache_engine {
  protected $memcache;
  protected $inited = FALSE;

  /**
   * Implementation of lambda_cache_engine::__construct().
   *
   * @param array $parameters
   */
  public function __construct(array $parameters) {
    $this->memcache = new Memcache;
    $this->inited = $this->memcache->connect($parameters['host'], $parameters['port']);
  }

  /**
   * Implementation of lambda_cache_engine::is_succeed().
   *
   * @return boolean
   */
  public function init_succeed() {
    return $this->inited;
  }

  /**
   * Implementation of lambda_cache_engine::get().
   *
   * @param string $key
   * @return mixed
   */
  public function get($key) {
    $val = $this->memcache->get($key);
    return $val === FALSE ? NULL : $val;
  }

  /**
   * Implementation of lambda_cache_engine::set().
   *
   * @param string $key
   * @param mixed $value
   */
  public function set($key, $value) {
    $this->memcache->set($key, $value);
  }

  /**
   * Implementation of lambda_cache_engine::flush().
   */
  public function flush(){
    $this->memcache->flush();
  }

  /**
   * Implementation of lambda_cache_engine::__destruct().
   */
  public function __destruct() {
    $this->memcache->close();
  }

  /**
   * Implementation of lambda_cache_engine::get_instance_parameters().
   *
   * @return array
   */
  public static function get_instance_parameters() {
    return array('host' => 'localhost', 'port' => 11211);
  }
}