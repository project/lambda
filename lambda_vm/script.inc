<?php

/**
 * Parser exception. It should be thrown by lambda_sciptengine::lexical_analysis().
 */
class LexicalAnalysisException extends UnexpectedValueException {}

/**
 * Parser exception. It should be thrown by lambda_sciptengine::syntactic_analysis().
 */
class SyntaxErrorException extends UnexpectedValueException {}

/**
 * Parser exception. It should be thrown by lambda_sciptengine::semantic_parsing().
 */
class ParseErrorException extends UnexpectedValueException {}

/**
 * Parser exception. It should be thrown by lambda_sciptengine::assemble_parse_tree().
 */
class ParseTreeDefectException extends UnexpectedValueException {}

/**
 * Parser exception. It should be thrown by lambda_sciptengine::get_parse_tree().
 */
class InvalidParseTreeException extends UnexpectedValueException {}

/**
 * Lambda scriptengine abstract class.
 *
 * The abstract part of this class just "suggests" a possible sequence of parsing.
 * You can do it in a completely way. That is why the internal parts are not defined.
 */
abstract class LambdaScriptEngine {

  /**
   * The raw source code.
   *
   * @var string
   */
  protected $source;

  /**
   * The parse tree, which is the result of assemble_parse_tree().
   *
   * @var LambdaParseTree
   */
  protected $parse_tree = NULL;

  /**
   * Constructor.
   *
   * @param string $source The raw source code.
   */
  public function __construct($source) {
    $this->source = (string)$source;
  }

  /**
   * Lexical analysis (tokenizing).
   *
   * This function splits the raw source code into tokens.
   */
  protected function lexical_analysis(){}

  /**
   * Syntactic analysis.
   *
   * This function checks if the tokens are in allowable expressions.
   */
  protected function syntactic_analysis(){}

  /**
   * Semantic parsing.
   *
   * This function "converts" the tokens to the appropirate functions.
   */
  protected function semantic_parsing() {}

  /**
   * Parse tree assembly.
   *
   * This function does the final conversions and alters to on the parse tree.
   *
   * Note: after this function, the internal validation function will check the parse tree.
   */
  protected abstract function assemble_parse_tree();

  protected final function validate_parse_tree() {
    // TODO implement validation
    if(FALSE) throw new InvalidParseTreeException();
  }

  /**
   * Returns the parse tree.
   *
   * @return LambdaParseTree
   */
  public function get_parse_tree() {
    $this->lexical_analysis();
    $this->syntactic_analysis();
    $this->semantic_parsing();
    $this->assemble_parse_tree();
    $this->validate_parse_tree();
    return $this->parse_tree;
  }

  // some api functions

  protected function setup_parse_tree($force = FALSE) {
    if(!$force && $this->parse_tree !== NULL) return;
    $this->parse_tree = new LambdaParseTree();
  }
}

// BIG FAT TODO: add value checking with PHP operator overloading (__set(), __get()).

class LambdaParseTreeObject {
  /**
   * Unique ID
   *
   * Note: it has noting to do with factorable::id.
   *
   * @var string
   */
  public $id = NULL;
}

class LambdaParseTree {
  /**
   * Modlet container.
   *
   * @var array
   */
  public $modlets = array();
}

class LambdaParseTreeModlet extends LambdaParseTreeObject {
  /**
   * Scene container
   *
   * @var array
   */
  public $scenes = array();
}

class LambdaParseTreeScene extends LambdaParseTreeObject {
  /**
   * Type of the scene. Only valid values are "hook" and "function".
   *
   * @var string
   */
  public $type = NULL;

  /**
   * List of parameters.
   *
   * @var array
   */
  public $parameters = array();

  /**
   * Content container.
   *
   * @var array
   */
  public $content = array();

  /**
   * List of connections.
   *
   * @var array
   */
  public $connections = array();
}

class LambdaParseTreeContent extends LambdaParseTreeObject {}

class LambdaParseTreeComponent extends LambdaParseTreeContent {

  /**
   * Type of the component.
   *
   * @var string
   */
  public $type = NULL;

  /**
   * Constructor parameters.
   *
   * @var array
   */
  public $constructor_parameters = array();
}

class LambdaParseTreeAction extends LambdaParseTreeContent {

  /**
   * Function name.
   *
   * @var string
   */
  public $function = NULL;

  /**
   * List of parameters.
   *
   * @var array
   */
  public $parameters = array();
}

class LambdaParseTreeConnection extends LambdaParseTreeObject {

  /**
   * Source of the connection.
   *
   * @var string
   */
  public $source = NULL;

  /**
   * Target object.
   *
   * @var string
   */
  public $target_obj = NULL;

  /**
   * Plug of the target object.
   *
   * @var string
   */
  public $target_plug = NULL;
}

class LambdaParseTreeParameter {

  /**
   * Paramter type.
   *
   * @var string
   */
  public $type = NULL;

  /**
   * Mixed value.
   *
   * @var mixed
   */
  public $value = NULL;
}

class ScriptEngineDescriptor {

  /**
   * Engine name.
   *
   * @var string
   */
  public $name;

  /**
   * Classname of the engine.
   *
   * @var class
   */
  public $class;

  /**
   * Constructor.
   *
   * @param string $name
   * @param class $class
   */
  public function __construct($name, $class) {
    $this->name = $name;
    $this->class = $class;
  }
}

function lambda_vm_get_script_engines() {
  static $engines = NULL;
  if($engines === NULL) {
    $engines = module_invoke_all('lambda_vm_register_script_engine');
  }
  return $engines;
}

function lambda_vm_get_script_engines_assoc() {
  $ret = array();
  foreach(lambda_vm_get_script_engines() as $se) {
    $ret[$se->class] = $se->name;
  }
  return $ret;
}