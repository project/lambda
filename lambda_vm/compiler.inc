<?php


/**
 * Compiles a parse tree into a tree of components and connections.
 */
class LambdaCompiler {
  
  /**
   * Unified parse tree
   *
   * @var LambdaParseTree
   */
  protected $parseTree;

  /**
   * List of modlets.
   *
   * @var array
   */
  protected $modlets;

  /**
   * Conversion dictionary between
   * the static and the dynamic
   * IDs.
   *
   * @var array
   */
  protected $dict = array();

  /**
   * Constructor.
   *
   * Note: this just initializes the data.
   *
   * @param LambdaParseTree $parseTree
   */
  public function __construct($parseTree) {
    $this->parseTree = $parseTree;
    $this->modlets = array();
  }

  /**
   * This function compiles
   * the parse tree into a tree of
   * lambda components and connections.
   */
  public function compile($forceRecompile = FALSE) {
    static $compiled = FALSE;
    if($forceRecompile) $compiled = FALSE;
    if($compiled) return;

    $this->iterateModlets();

    $compiled = TRUE;
  }

  /**
   * Iterates on the modlets.
   */
  protected function iterateModlets() {
    foreach($this->parseTree->modlets as $m) {
      $this->processModlet($m);
    }
  }

  /**
   * Processes a LambdaParseTreeModlet into a Modlet.
   *
   * @param LambdaParseTreeModlet $modlet
   * @return Modlet
   */
  protected function processModlet(LambdaParseTreeModlet $modlet) {
    $m = factory::new_instance('Modlet', array()); // TODO fix parameters
    $this->modlets []= $m;
    $this->dict[$modlet->id] = $m->get_id();
    foreach($modlet->scenes as $s) {
      $scene = $this->processScene($s);
      $m->addScene($scene);
    }
    return $m;
  }

  protected function processScenes(LambdaParseTreeScene $scene) {
    ;
  }

  protected function processConnections(LambdaParseTreeConnection $connection) {
    ;
  }
}