<?php

/**
 * Caching interface for the lambda VM
 */
interface lambda_cache_engine {

  /**
   * Constructs an instance of the cache object.
   *
   * @param array $parameters associative array of the parameters
   */
  public function __construct(array $parameters);

  /**
   * Returns the result of the initiation. If it returns FALSE,
   * then the cache won't be used at the page request.
   *
   * @return boolean
   */
  public function init_succeed();

  /**
   * Returns an unserialized value from the cache.
   * If it fails, it returns NULL
   *
   * @param string $key
   * @return mixed
   */
  public function get($key);

  /**
   * Stores a value in the cache.
   *
   * Note: NULL values won't be passed to this function.
   *
   * @param string $key
   * @param mixed $value
   */
  public function set($key, $value);

  /**
   * Flushes the cache.
   */
  public function flush();

  /**
   * Destructor function.
   *
   * Here, you should close the connection.
   * Note: you don't have to flush here, because a flush
   * will be called just before the unset()
   */
  public function __destruct();

  /**
   * Returns the parameters which is required to
   * create the instance of the cache class.
   *
   * @return array
   * List of the parameters
   */
  public static function get_instance_parameters();
}

class lambda_cache {
  protected static $inited = FALSE;
  protected static $available_engines = NULL;
  protected static $engines = NULL;

  /**
   * Initializes the caches.
   */
  protected static function init() {
    if(self::$inited) return;
    self::$available_engines = module_invoke_all('lambda_vm_register_cache_engine');
    
    self::$engines = array();
    $res = db_query('SELECT type, arguments, weight FROM {lambda_vm_cache_engines} ORDER BY weight ASC');
    while($row = db_fetch_array($res)) {
      if(in_array($row['type'], self::$available_engines)) {
        $type = $row['type'];
        $e = new $type(unserialize($row['arguments']));
        if($e->init_succeed()) {
          self::$engines []= $e;
        }
      }
    }

    self::$inited = TRUE;
  }

  /**
   * Gets an item from the cache pool.
   *
   * @param string $key
   * @return mixed
   */
  public static function get($key) {
    if(!self::$inited) self::init();
    foreach(self::$engines as $e) {
      $ret = $e->get($key);
      if($ret !== NULL)
        return $ret;
    }
    return NULL;
  }

  /**
   * Stores a value in ALL caches.
   *
   * @param string $key
   * @param mixed $value
   */
  public static function set($key, $value) {
    if($value === NULL) return; // why do we want to store NULL?
    if(!self::$inited) self::init();
    foreach(self::$engines as $e) {
      $e->set($key, $value);
    }
  }

  /**
   * Flushes all caches.
   */
  public static function flush() {
    if(!self::$inited) self::init();
    foreach(self::$engines as $e)
      $e->flush();
  }

  /**
   * Shuts down all caches.
   */
  public static function close() {
    if(!self::$inited) return;
    foreach(array_keys(self::$engines) as $k) {
      self::$engines[$k]->flush();
      unset(self::$engines[$k]);
    }
  }

  /**
   * Returns the number of the active cache engines.
   *
   * @return int
   */
  public static function get_engines_count() {
    if(!self::$inited) self::init();
    return count(self::$engines);
  }
  
  private function __construct(){
  }
}