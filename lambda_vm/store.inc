<?php

interface LambdaStore {
  public function getIOPluginClassname($id);

  // raw access functions (these may be removed or changed later during refactorizations)
  public function load($id);
  public function save($id, $source);
  public function delete($id);
  public function getRawSource($id);

  // a bit more sophisticated ones :)
  public function getModletList();
}

class LambdaStoreDescriptor {
  public $classname;
  public $name;

  function __construct($classname, $name) {
    $this->classname = $classname;
    $this->name = $name;
  }
}

function lambdastore_get_store_providers() {
  static $cache = NULL;
  if($cache === NULL) {
    $cache = module_invoke_all('lambdastore_register_provider');
  }
  return $cache;
}

function _lambdastore_get_store_providers_assoc() {
  $ret = array();
  foreach(lambdastore_get_store_providers() as $p) {
    $ret[$p->classname] = $p->name;
  }
  return $ret;
}

function _lambdastore_adminform() {
  $form = array();

  $form['lambda_storage_engine'] = array(
      '#type' => 'select',
      '#options' => array_merge(array('0' => 'No engine'), _lambdastore_get_store_providers_assoc()),
      '#default_value' => variable_get('lambda_storage_engine', '0'),
  );

  return system_settings_form($form);
}