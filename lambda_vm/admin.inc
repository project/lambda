<?php

/**
 * Admin page form.
 *
 * @return array
 */
function _lambda_vm_admin_page_general() {
  $form = array();

  $form['lambda_vm_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode'),
    '#default_value' => variable_get('lambda_vm_debug', FALSE),
  );

  $form = system_settings_form($form);
  
  array_unshift($form['#submit'], '_lambda_vm_tag_force_rebuild');
  array_push($form['#submit'], '_lambda_vm_force_rebuild');

  return $form;
}

function _lambda_vm_set_rebuild($val = NULL) {
  static $cache = FALSE;
  if($val === NULL) return $cache;
  else $cache = $val;
}

function _lambda_vm_get_rebuild() {
  return _lambda_vm_set_rebuild();
}

/**
 * Helper function for the admin page form.
 *
 * @param array $form
 * @param array $form_state
 */
function _lambda_vm_tag_force_rebuild($form, $form_state) {
  if($form_state['values']['lambda_vm_debug'] != variable_get('lambda_vm_debug', FALSE)) {
    _lambda_vm_set_rebuild(TRUE);
  }
}

function _lambda_vm_force_rebuild() {
  if(_lambda_vm_get_rebuild()) {
    drupal_flush_all_caches();
  }
}

function _lambda_vm_admin_modlets() {
  $form = array();
  return $form;
}

function _lambda_vm_admin_modlets_submit($form, $from_state) {
  ;
}