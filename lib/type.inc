<?php

abstract class _type {
	
  /**
   * Returns the classname.
   *
   * @return string 
   */
  public static final function get_classname() {
		return self::get_name().'_type';
	}

	/**
	 * The machine readable name of the type.
	 *
	 * By convention, it should be "CLASSNAME" from
	 * class CLASSNAME_type extends _type
	 *
	 * @return string
	 */
	public abstract static function get_name();

	/**
	 * Decides wherther $var is the instance of
	 * this type or not.
	 *
	 * @param any $var
	 * @return boolean
	 */
	public abstract static function is_me($var);

	/**
	 * Decides whether $var can be converted
	 * to an instance of this type.
	 *
	 * @param any $var
	 * @return boolean
	 */
	public abstract static function is_convertable($var);

	/**
	 * Tries to convert $var to an instance of
	 * this type.
	 *
	 * @param any $type
	 * @return instance or null
	 */
	public abstract static function convert($type);
}
