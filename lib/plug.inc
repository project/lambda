<?php

/**
 * plug class
 *
 * Information and toolbox class which guides the variables through the pipes.
 */
class plug extends factorable {
	protected $name;
	protected $types;
	protected $value;
	private $assigned = FALSE;

  /**
   * Returns the name of the plug
   *
   * The name is used to identify the plug in it's connectable class.
   *
   * @return string
   */
	public function get_name() {
		return $this->name;
	}

  /**
   * Returns types of this plug.
   *
   * @return _type
   */
	public function get_type() {
		return $this->type;
	}

  /**
   * Constructor of the plug.
   *
   * @param string $name
   * Every plug can holds a "name" which is just a caption
   * @param array $types
   * Every plug can accept multiple types. This list can be set only at
   * the __constuct() function
   */
	public function __construct($name, array $types) {
		$this->name = $name;
		$this->types = array();
		foreach($types as &$t)
			if($t instanceof _type)
				$this->types[] = $t;
	}

  /**
   * Returns TRUE if this plug already has a value.
   *
   * @return boolean
   */
	public function has_value() {
		return $this->assigned;
	}

  /**
   * Returns the value of this plug
   *
   * @return any
   */
	public function get_value() {
		if($this->assigned)
			return $this->value;
		return NULL;
	}

	private function in_types(&$var) {
		foreach($this->types as &$t)
			if(is_a($var, $t->get_classname()))
				return TRUE;
		return FALSE;
	}

  /**
   * Assigns a value to this plug.
   *
   * @param any $val
   */
	public function assign_value($val) {
		if($this->assigned) return;
		if(!$val instanceof _type) return;

		if($this->in_types($val)) {
			$this->value = $val;
			$this->assigned = TRUE;
			return;
		}

		foreach($this->types as &$t)
      if($t->is_convertable($val)) {
        $this->value = $t->convert($val);
        $this->assigned = TRUE;
        return;
      }
	}
}
