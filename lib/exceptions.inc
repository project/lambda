<?php

class ClassNotFoundException extends RuntimeException {}
class ClassInstantiationErrorException extends RuntimeException {}