<?php

include_once 'connectable.inc';
include_once 'gui_info.inc';

/**
 * Variable class.
 *
 * This is the superclass of all variable connectables.
 */
abstract class variable extends connectable implements gui_info {
  /**
   * This is the value of the variable.
   *
   * This must be set in the __construct() method.
   *
   * @var mixed
   */
  protected $value;
  protected static $use_static_cache = TRUE;
  protected static $immutable = TRUE;

  public function output() {
    return $value;
  }
}