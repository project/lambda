<?php

include_once 'commons.inc';

/**
 * This class indicates that the subclass
 * can be created and stored by the factory class.
 */
abstract class factorable {
  private $id = NULL;
  
  /**
   * We encourage you to use named arguments on your connectables.
   *
   * The only way to use them easily in PHP is the associative array.
   * You should use it as a dictionary with key-value pairs.
   *
   * @param array $arguments a dictionary (associative array) of the named arguments
   */
  public abstract function __construct(array $arguments);

  /**
   * Return the unique ID of the class.
   * @see set_id
   *
   * @return string unique ID
   */
  public final function get_id() {
    return $this->id;
  }
  
  /**
   * Sets the unique ID of the class.
   *
   * @param string $id
   */
  public final function set_id($id) {
    if($this->id === NULL) {
      $this->id = $id;
    }
  }
}

final class factory {
  private static $reg = array();

  /**
   * Creates a new class with an unique ID.
   *
   * @param string $class
   * The name of the class type. The class must be a derivate of the
   * factorable class.
   * @param array $arguments associative array of named arguments
   * @return factorable $instance
   */
  public static function new_instance($class, array $arguments) {
    $id = self::generate_random_id();
    try {
      $inst = n($class, $arguments);
    } catch (Exception $e) {
      return NULL;
    }
    if($inst instanceof factorable) {
      $inst->set_id($id);
      self::$reg[$id] = $inst;
      return $inst;
    }
    return NULL;
  }

  /**
   * Returns an item.
   *
   * @param string $id unique id of the item
   * @return factorable the item
   */
  public static function get_item($id) {
    return isset(self::$reg[$id]) ? self::$reg[$id] : NULL;
  }

  /**
   * Generates a random and unique id.
   * This is just a helper method for new_instance()
   *
   * @return string ID
   */
  private static function generate_random_id() {
    $id = NULL;
    do {
      $id = sha1(mt_rand());
    } while(in_array($id, array_keys(self::$reg)));
    return $id;
  }

  /**
   * Returns a subset of the stored objects.
   *
   * @param function $func
   * This function has 2 criteria:
   *  - it must have exactly one argument which takes the item as a parameter
   *  - it must return a boolean value
   * @return array
   */
  public static function filter($func) {
    $ret = array();
    if(function_exists($func))
      foreach(self::$reg as $r)
        if($func($r))
          $ret []= $r;
    return $ret;
  }

  private function __construct(){
  }
}
