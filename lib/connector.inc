<?php

final class connector {
  private static $connections = array();

  /**
   * Connects two components.
   *
   * @param connectable $src_component
   * The component which has the INPUT plug.
   * @param string $input_plugname
   * The identifier of the input plug.
   * @param connectable $dest_component
   * The component which has the OUTPUT plug.
   */
  public static function connect($src_component, $input_plugname, $dest_component) {
    if(!isset(self::$connections[$src_component]))
      self::$connections[$src_component] = array();
    self::$connections[$src_component][$input_plugname] = $dest_component;
  }

  /**
   * Returns the output connectable of an input plug
   *
   * @param connectable $src_component
   * @param string $input_plugname
   * @return connectable
   */
  public static function get_output_connectable($src_component, $input_plugname) {
    return (isset(self::$connections[$src_component]) && isset(self::$connections[$src_component][$input_plugname])) ?
      self::$connections[$src_component][$input_plugname] : NULL;
  }

  private function __construct(){
  }
}
