<?php

include_once 'connectable.inc';

abstract class scene extends connectable {
  /**
   * List of the inner plugs.
   *
   * @var array
   */
  protected $inner_plugs = array();

  /**
   * Implements the connectable::output().
   */
  protected function output() {
    $this->fetch_inner_plugs();
    $this->scene_output();
  }

  /**
   * Abstract function, which holds the business logic
   * of the scene manager.
   */
  protected abstract function scene_output();

  /**
   * Gathers the values of the inner plugs.
   */
  private function fetch_inner_plugs() {
    foreach($this->inner_plugs as $ip) {
      if(!$ip->has_value()) {
        $outplug = connector::get_output_connectable($this->get_id(), $ip->get_id());
        if($outplug !== NULL)
          $ip->assign_value($outplug->get_output());
      }
    }
  }
}