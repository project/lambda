<?php

interface gui_info {
  /**
   * Returns the required form fields to construct a class.
   *
   * @return array
   * The structure is the same as the formapi has. For further details,
   * see the formapi's documentation.
   */
  public static function get_constructor_parameters();

  /**
   * Validates the constructor parameters.
   *
   * @return array
   * array with the list of the wrong elements.
   * Empty array means that the validation is passed.
   *
   * @param array $values
   * @return array
   */
  public static function validate_constructor_parameters($values);
}