<?php

include_once 'factory.inc';

/**
 * Connectable class
 *
 * This class represents an object which can be connected to a different object.
 *
 * I'm sure that you don't want to use this class directly. Use a subclass of it.
 */
abstract class connectable extends factorable {
	protected $input_plugs = array();
  protected $id = NULL;
  protected $output_value;
  protected $calculated = FALSE;

  /**
   * Controls the caching of this object.
   *
   * If this is enabled then this object will only call the output() function once.
   * This can incrase the performance, however in some situations you might want to turn it off.
   *
   * @var boolean
   */
  protected static $use_static_cache = TRUE;

  /**
   * Controls the immutable state of the object.
   *
   * You can set this TRUE if you are 100% percent sure that your object
   * does not have any side effect. In this case the VM may use agressive caching,
   * which probably lead to better performance.
   *
   * If you are unsure, or you don't understand what does it mean exactly,
   * you want to leave it on FALSE.
   *
   * @var boolean
   */
  protected static $immutable = FALSE;

  /**
   * Returns the immutable state of this class.
   * @see immutable
   *
   * @return boolean
   */
  public static function is_immutable() {
    return (bool)self::$immutable;
  }

  /**
   * This abstract function holds the business logic of your connectable.
   */
	protected abstract function output();

	/**
   * Returns the output of this object.
   */
  public final function get_output() {
    // check for caching
    if(self::$use_static_cache && $this->calculated) return $this->output_value;
    // otherwise do the calculation
    $this->fetch_input_plugs();
    $this->calculated = TRUE;
    $this->output_value = $this->output();
    return $this->output_value;
	}

  /**
   * Gathers the values for the input plugs.
   */
	private function fetch_input_plugs() {
		foreach($input_plugs as $ip) {
      if(!$ip->has_value()) {
        $outplug = connector::get_output_connectable($this->get_id(), $ip->get_id());
        if(!$outplug === NULL) {
          $ip->assign_value($outplug->get_output());
        }
      }
		}
	}

  /**
   * Returns the list of the input plugs
   *
   * @return array
   */
	public final function get_input_plugs() {
		return $this->input_plugs;
	}
}
