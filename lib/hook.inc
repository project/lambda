<?php

include_once 'scene.inc';

/**
 * Bean class to describe a hook parameter.
 */
class hook_parameter {
  
  /**
   * The type of the parameter. It will be raised as
   * $typeobj = $type.'_type';
   * $x = new $typeobj;
   *
   * @var string
   */
  protected $type = NULL;

  /**
   * This indicates whether the parameter a reference or a value.
   *
   * @var boolean
   */
  protected $reference = NULL;

  /**
   * The name of the parameter.
   * It is only for documentational purposes, but important.
   *
   * @var string
   */
  protected $name = NULL;

  /**
   * Constructor.
   *
   * @param string $name @see name
   * @param string $type @see type
   * @param boolean $reference @see reference
   */
  public function __construct($name = NULL, $type = NULL, $reference = NULL) {
    $this->type = $type === NULL ? NULL : (string)$type;
    $this->reference = $reference === NULL ? NULL : (bool)$reference;
    $this->name = $name === NULL ? NULL : (string)$name;
  }

  /**
   * Returns the type of the parameter.
   *
   * @return string
   */
  public function get_type() {
    return $this->type;
  }

  /**
   * Sets the type if it is not set.
   *
   * @param string $type
   */
  public function set_type($type) {
    if($this->type === NULL)
      $this->type = (string)$type;
  }

  /**
   * Returns TRUE if the parameter is a reference.
   *
   * @return boolean
   */
  public function is_reference() {
    return $this->reference;
  }

  /**
   * Sets the reference state of the parameter if it is not set.
   *
   * @param bool $reference
   */
  public function set_reference($reference) {
    if($this->reference === NULL)
      $this->reference = (bool)$reference;
  }

  /**
   * Returns the name of the parameter.
   *
   * @return string
   */
  public function get_name() {
    return $this->name;
  }

  /**
   * Sets the name of the parameter if it is not set.
   *
   * @param string $name
   */
  public function set_name($name) {
    if($this->name === NULL)
      $this->name = (string)$name;
  }
}

/**
 * Bean class to describe a hook.
 */
class hook {
  /**
   * Holds a list of the parameters.
   *
   * @var array
   */
  protected $parameters = NULL;

  /**
   * Return type. It must a valid type object.
   * It will be constructed like this:
   * $rettype = $return_type . '_type';
   * $retobj = new $rettype;
   *
   * @var string
   */
  protected $return_type = NULL;

  /**
   * Constructor.
   *
   * @param array $parameters
   * @param string $return_type
   */
  public function __construct(array $parameters = NULL, $return_type = NULL) {
    $this->parameters = $parameters;
    $this->return_type = $return_type;
  }

  /**
   * Returns an array of hook_parameters
   *
   * @return array
   */
  public function get_parameters() {
    return $this->parameters;
  }

  /**
   * Sets the parameters if it is not set.
   *
   * @param array $parameters
   */
  public function set_parameters(array $parameters) {
    if($this->parameters === NULL)
      $this->parameters = $parameters;
  }

  /**
   * Returns the name of the return type.
   *
   * @return string
   */
  public function get_return_type() {
    return $this->return_type;
  }

  /**
   * Sets the return type.
   *
   * @param string $return_type
   */
  public function set_return_type($return_type) {
    if($this->return_type === NULL)
      $this->return_type = $return_type;
  }
}

/**
 * Hook manager interface.
 *
 * This interface allows a bit more sophisitcated way
 * to store hooks. Instead of static hooks, it allows
 * dynamic lookup for the specified hook, which is
 * a huge speed boost.
 *
 * A very good example is a hook store in the database.
 * You don't have to get all hooks, just the one you need.
 */
interface hook_manager extends lambda_sortable {

  /**
   * Searches for a hook.
   *
   * @param string $name
   */
  public function lookup($name);
}

class scene_hook extends scene {

  /**
   * Array of the statically registered hooks.
   *
   * @var array
   */
  protected static $hooks = NULL;

  /**
   * Sorted array of the hook manager classes.
   *
   * @var array
   */
  protected static $hook_managers = array();

  /**
   * The current hook.
   *
   * @var hook
   */
  protected $hook = null;

  /**
   * Constructor.
   *
   * @param array $arguments
   */
  public function __construct($arguments) {
    self::get_registered_hooks();
    $this->hook = self::lookup_hook($arguments['name']);
  }

  /**
   * Does a lookup for a hook.
   *
   * @param string $name
   *   The name of the hook.
   * @return hook
   * @throws UnexpectedValueException
   *   When the hook is not found.
   */
  protected static function lookup_hook($name) {
    if(isset(self::$hooks[$name])) return self::$hooks[$name];
    foreach(self::$hook_managers as $m) {
      $candidate = $m->lookup($name);
      if($candidate !== NULL && $candidate instanceof hook)
        return $candidate;
    }
    throw new UnexpectedValueException(t('Unsupported hook'));
  }

  /**
   * Returns the list of the statically registered hooks.
   *
   * @return array
   */
  public static function get_registered_hooks() {
    if(self::$hooks === NULL)
      self::$hooks = module_invoke_all('lambda_register_hook');
    return self::$hooks;
  }

  /**
   * Registers a new hook manager.
   *
   * @param hook_manager $manager
   */
  public static function add_hook_manager(hook_manager $manager) {
    self::$hook_managers []= $manager;
    if(!usort(self::$hook_managers, 'lambda_sortable_comparator'))
      throw new RuntimeException(t('usort failed'));
  }

  protected function scene_output() {
  }
  
}