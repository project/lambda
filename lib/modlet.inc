<?php

class Modlet extends connectable {
  protected $scenes = array();
  
  public function __construct($arguments) {
  }
  
  protected function output() {
  }

  public function addScene(scene $scene) {
    if(!isset($this->scenes[$scene->get_id()]))
      $this->scenes[$scene->get_id()] = $scene;
  }

  public function getScene($id) {
    return isset($this->scenes[$id]) ? $this->scenes[$id] : NULL;
  }
  
}