<?php

include_once 'exceptions.inc';

/**
 * Construct a class, but if an error occoures,
 * it throws an exception instead of printing out
 * error messages.
 * @todo if anyone knows a better solution for this, send me a patch, thx
 *
 * @param string $classname
 * @param array $args
 * @return class
 */
function new_class($classname, $args) {
  if(!class_exists($classname)) throw new ClassNotFoundException();
  if(!is_array($args)) throw new InvalidArgumentException();
  $params = '';
  foreach($args as $k=>$v) 
    $params .= "\$args['$k'],";
  $params = rtrim($params, ',');
  eval("\$ret = new $classname($params);");
  if(!is_object($ret)) throw new ClassInstantiationErrorException();
  return $ret;
}

/**
 * Shorthand function for new_class().
 *
 * Usage:
 * $myobj = n('myclass', $param0, $param1, ..., $paramN);
 * @see new_class
 *
 * @param string $classname
 * @return class
 */
function n($classname) {
  $args = func_get_args();
  array_shift($args);
  return new_class($classname, $args);
}

/**
 * Sortable interface for the lambda module.
 *
 * @see lambda_sortable_comparator
 */
interface lambda_sortable {
  public function get_weight();
}

/**
 * Callback function for usort() or uasort().
 *
 * This function compares lambda_sortable objects,
 * based on their weight.
 *
 * @see usort
 * @see uasort
 *
 * @param lambda_sortable $s0
 * @param lambda_sortable $s1
 * @return int
 */
function lambda_sortable_comparator(lambda_sortable $s0, lambda_sortable $s1) {
  if($s0->get_weight() == $s1->get_weight()) return 0;
  return ($s0->get_weight() < $s1->get_weight()) ? -1 : 1;
}
