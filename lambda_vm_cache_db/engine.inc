<?php

class db_lambda_cache_engine implements lambda_cache_engine {
  protected static $ids = array();
  protected $init_succeed = FALSE;
  protected $id = NULL;
  protected $hibernated = array();

  /**
   * Implementation of lambda_cache_engine::__construct().
   *
   * @param array $parameters
   */
  public function __construct(array $parameters) {
    $id = (int)$parameters['id'];
    if(in_array($id, self::$ids)) return; // resource has been already allocated
    $this->id = $id;
    self::$ids []= $id;
    $this->init_succeed = TRUE;
  }

  /**
   * Implementation of lambda_cache_engine::is_succeed().
   *
   * @return boolean
   */
  public function init_succeed() {
    return $this->init_succeed;
  }

  /**
   * Implementation of lambda_cache_engine::get().
   *
   * @param string $key
   * @return mixed
   */
  public function get($key) {
    if(isset($this->hibernated[$key]))
      return $this->hibernated[$key];
    $row = db_fetch_array(db_query('SELECT "value" FROM {lambda_vm_cache_db} WHERE "id" = %d AND "key" = \'%s\'', $this->id, $key));
    if(!$row) return NULL;
    $val = unserialize($row['value']);
    $this->hibernated[$key] = $val;
    return $val;
  }

  /**
   * Implementation of lambda_cache_engine::set().
   *
   * @param string $key
   * @param mixed $value
   */
  public function set($key, $value) {
    $this->hibernated[$key] = $value;
    $serialized = serialize($value);
    db_query('UPDATE {lambda_vm_cache_db} SET "value" = \'%s\' WHERE id = %d AND "key" = \'%s\'', $serialized, $this->id, $key);
    if(db_affected_rows() == 0) // oops, the cache entry is not in the database yet
      db_query('INSERT INTO {lambda_vm_cache_db}(id, "key", "value") VALUES(%d, \'%s\', \'%s\')', $this->id, $key, $serialized);
  }

  /**
   * Implementation of lambda_cache_engine::flush().
   */
  public function flush(){
  }

  /**
   * Implementation of lambda_cache_engine::__destruct().
   */
  public function __destruct() {
  }

  /**
   * Implementation of lambda_cache_engine::get_instance_parameters().
   *
   * @return array
   */
  public static function get_instance_parameters() {
    return array('id' => 0);
  }
}